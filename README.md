# Organization

### Schedule for the seminar

Date | Topic | Paper | Person | Cake
---- | ----- | ----- | ------ | ---- 
21.10 | Overview | Fan et al'19| | Miguel
28.10 | Energy Landscape | Choromanska et al'15 | Gytis |
4.11 | Mean field | Met at al'18 | |
11.11 | Implicit regularization | Gunasekar et al'19| Samuel |
18.11 | Implicit regularization | Arora et al'19a| Karolina |
25.11 | Optimization | Arora et al'19b |Julia  |
2.12 | Exact interpolation | Belkin et al'18 | |
9.12 | Time series|  | Amelie |
6.1 | Classification | Kim et al'18 | Leo Weber |
13.1 | Nonparametric regression  | Schmidt-Hieber'18 | Shayan |
20.1 | Entropy bounds| Bartlett et al'17 | Rupsa |
27.1 | Training & regression | Dou, Liang'19 | |
3.2 | GANs| Liang'18 | | 
